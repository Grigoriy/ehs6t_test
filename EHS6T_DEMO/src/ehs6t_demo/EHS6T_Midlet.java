/*
 * Test program for EHS6T Terminal.
 * Accepts CSD connections and transmits all received data to serial port.
 * Serial Port baudRate is 9600 bits/second. 
 */

package ehs6t_demo;

import com.cinterion.io.ATCommand;
import com.cinterion.io.ATCommandFailedException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.microedition.io.CommConnection;
import javax.microedition.io.Connector;
import javax.microedition.midlet.*;

/**
 * @author Grioriy Kudinov
 */
public class EHS6T_Midlet extends MIDlet {
    
    private  ATCommand dataCmd;
   

    public void startApp() {
        
        System.out.println("Application starts v 30.11 23-19");

        try {
            dataCmd = new ATCommand(true);
            dataCmd.addListener(new EventListener(dataCmd));
            String str;
            // использовать 2G вместо 3G (нужно для нормальной работы csd c 2G терминалами на другой стороне)
            str = dataCmd.send ("AT^SXRAT=0,0\r");
            System.out.println(str);
            // выключить sleep режим для uart (иначе перестает воспринимать команды)
            str = dataCmd.send ("AT^SPOW=1,0,0\r");
            System.out.println(str);
            
            str = dataCmd.send ("AT+CBST=71,0,1\r");
            System.out.println(str);// скорость CSD 9600
            str = dataCmd.send ("AT+CLIP=1\r");System.out.println(str); //показывать URC CLIP
            str = dataCmd.send ("AT+CRC=1\r");System.out.println(str); //показывать тип вызова (голос или данные)
            
            str = dataCmd.send ("ATS0=0\r");System.out.println(str); //выкл автоответ
            str = dataCmd.send ("AT&C1\r");System.out.println(str); //DCD line is ON in the presence of data carrier only
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        System.out.println("Application running. Wait for incoming calls.");
        while(true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void pauseApp() {
    }
    
    public void destroyApp(boolean unconditional) {
        try {
            dataCmd.release();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
    }
}
