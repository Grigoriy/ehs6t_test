package ehs6t_demo;

import com.cinterion.io.*;

/**
 * Starts connection thread when incoming call received.
 * @author Grigory Kudinov
 */
public class EventListener implements ATCommandListener {
    private ATCommand dataAtCommand;

    EventListener(ATCommand dataCmd) {
        this.dataAtCommand = dataCmd;
    }

    public void ATEvent(String event) {
        System.out.println("AT event: " + event);
        
        if (event.indexOf("+CLIP") >= 0) {
                System.out.println("+CLIP detected.");
                Thread userThread = new DataReceiverThread(dataAtCommand);
                
                System.out.println("Starting receiverThread");
                userThread.start();
            }
    }

    public void RINGChanged(boolean bln) {
        // do nothing
    }

    public void DCDChanged(boolean bln) {
        // do nothing
    }

    public void DSRChanged(boolean bln) {
        // do nothing
    }

    public void CONNChanged(boolean bln) {
        // do nothing
    }
    
}
