package ehs6t_demo;

import com.cinterion.io.ATCommand;
import com.cinterion.io.ATCommandFailedException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Thread class for receiving CSD data and retransmitting it to serial out.
 * @author Grigoriy Kudinov
 */
class DataReceiverThread extends Thread {
    private ATCommand dataAtCommand;

    public DataReceiverThread() {
    }

    DataReceiverThread(ATCommand dataAtCommand) {
        this.dataAtCommand = dataAtCommand;
    }
    
    public void run() {
        try {
            System.out.println("Try to accept call.");
            String str = dataAtCommand.send("ATA" + "\r");
            System.out.println(str);
            if (str.indexOf("CONNECT") >= 0) {
                System.out.println("Connected.");
                InputStream dataIn = dataAtCommand.getDataInputStream();
                byte[] buf = new byte[1024];
                while (true) {                    
                    int read = 0;
                    while (true) {
                        //sleep(10);
                        while (dataIn.available() > 0) {
                            read = dataIn.read(buf);
                            System.out.print(new String(buf, 0, read));
                            // buf = new byte[1024];
                        }
                    }
                }
            } else {
                System.out.println("Connect failed.");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}
